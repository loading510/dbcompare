 **数据库对比工具!** 

 **运行环境：** 
.NETFramework4.5


- 1、配置主副库的信息
![配置主，副库信息](http://git.oschina.net/uploads/images/2017/0314/161749_578e6168_66542.png "在这里输入图片标题")
- 2、分析后，自动高亮显示出两个库中数据差异的表。
![输入图片说明](http://git.oschina.net/uploads/images/2017/0314/162224_205607b9_66542.png "在这里输入图片标题")
- 3、选择需要改变的表或者字段进行操作
![输入图片说明](http://git.oschina.net/uploads/images/2017/0314/162731_7b98f050_66542.png "在这里输入图片标题")

近期有闲准备升级一下
1. 支持MSSQL
1. 优化性能
1. 改善显示界面，使UI更友好，美观。
大家如果还想加入什么需要的功能可以留言，我看看能不能加进去。。。